#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/opencv.hpp>
#include<math.h>
#include <float.h>

//ImageEnhancementHeader
bool NRCIR2009(cv::Mat ori, cv::Mat &ret);
bool MSRCP2014(const cv::Mat &src, cv::Mat &dst);
bool LeeLeeKim2013(const cv::Mat ori, cv::Mat &ret, double alpha);

//Auxiliary
bool SimplestColorBalance(cv::Mat ori, float upperthresh, float lowerthresh);