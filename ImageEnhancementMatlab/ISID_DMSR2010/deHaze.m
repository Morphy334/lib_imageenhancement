function J = deHaze(im,varargin)
try
    gaussianstd=varargin{1};
    C=varargin{2};
catch
    gaussianstd=[3,13,41];
    C=1.08;
end

[height,width,~]=size(im);
im = double(im);
im = im./255;
JDark = darkChannel(im);
A = atmLight(im, JDark);
YCbCr = rgb2ycbcr(im);
Y=YCbCr(:,:,1);
% figure ;imshow(Y);

scaleNum=3;
scaleWeight=1.0/scaleNum;

H=zeros(height,width,3);
Yblurred=zeros(height,width,3);
Rm=zeros(height,width,1);
for index=1:scaleNum
    H = fspecial('gaussian',gaussianstd(index)*6,gaussianstd(index));
    Yblurred(:,:,index) = imfilter(Y,H,'replicate');
end


for index=1:scaleNum
    Rm=Rm+scaleWeight*(log(Y+0.0001)-log(Yblurred(:,:,index)));
end

amplifyMap=Rm+1;
Rm=Y.*amplifyMap;
Rm=min(Rm,1);
Rm=max(Rm,0);

RmRefined1=C-Rm;
transmission = medfilt2(RmRefined1,[21,21]);
% figure;imshow(transmission);colormap gray;colorbar;title('transmission');

tMat = transmission;
J = getRadiance(A,im,tMat);
