function A = a_atmLight(im, JDark)

% the color of the atmospheric light is very close to the color of the sky
% so just pick the first few pixels that are closest to 1 in JDark
% and take the average

% pick top 0.1% brightest pixels in the dark channel

% get the image size
[height, width, ~] = size(im);
imsize = width * height;

numpx = floor(imsize/1000); % accomodate for small images
JDarkVec = reshape(JDark,imsize,1); % a vector of pixels in JDark %是一個315000(525*600)*1的二維矩陣
ImVec = reshape(im,imsize,3);  % a vector of pixels in my image %是一個315000(525*600)*3的二維矩陣 第一行即R 第二行G 第三行 B

[JDarkVec, indices] = sort(JDarkVec); %sort  %排列JDarkVec(即dark channel)的值，由小排到大 並抓出其相對應的index
%目前indices是315000*1
indices = indices(imsize-numpx+1:end); % need the last few pixels because those are closest to 1 %只抓後面imsize-numpx+1:end個的像素
%目前indices是315*1

atmSum = zeros(1,3);
for ind = 1:numpx
    atmSum = atmSum + ImVec(indices(ind),:); %把最大的幾個像素(imsize-numpx+1:end)加起來
end

A = atmSum / numpx;%並平均，當作A，A是1*3*1
