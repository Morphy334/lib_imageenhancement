close all
clear all
clc
im=imread('fogtest.jpg');
J = anEfficient(im,3,45,0.1);
figure;
subplot(1,2,1);
imagesc(im)
title 'Original'
axis image off;
subplot(1,2,2);
imagesc(J)
title 'De-hazed'
axis image off;