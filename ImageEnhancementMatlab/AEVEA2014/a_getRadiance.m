function J = a_getRadiance(A,im,tMat,gamma,sigma)
t0 = 0.1;
J = zeros(size(im));
for ind = 1:3
    
%     J(:,:,ind) = (im(:,:,ind) - A(ind))./max(tMat,t0)+A(ind)  ; 
   J(:,:,ind) = gamma(ind)*(im(:,:,ind) - A(ind))./max(tMat,t0)+A(ind) +gamma(ind)*(gamma(ind)-1) ; 

end

J = J./(max(max(max(J))));