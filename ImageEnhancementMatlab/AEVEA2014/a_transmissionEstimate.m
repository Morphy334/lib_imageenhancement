function transmission = a_transmissionEstimate(im, A,patchSize)



im3 = zeros(size(im));
for ind = 1:3 
    im3(:,:,ind) = im(:,:,ind)./A(ind);%式子(12)的Ic(y)/Ac
end

% imagesc(im3./(max(max(max(im3))))); colormap gray; axis off image

transmission = a_darkChannel(im3,patchSize);%式子(12)
