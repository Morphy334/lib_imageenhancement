function J = anEfficient(im,varargin)

try
    alpha=varargin{1};
    beta=varargin{2};
    omega =varargin{3};
catch
    alpha=3;
    beta=45;
    omega = 0.95; % the amount of haze we're keeping
end

[height,width,~]=size(im);
im = double(im);
im = im./255;
JDark_s = alpha/(alpha+beta)*a_darkChannel(im,alpha);
JDark_l = beta/(alpha+beta)*a_darkChannel(im,beta);
JDark=JDark_s+JDark_l;
A = a_atmLight(im, JDark);

transmission_s = a_transmissionEstimate(im, A,alpha);
transmission_l = a_transmissionEstimate(im, A,beta);
transmission=1-omega*alpha/(alpha+beta)*transmission_s-omega*beta/(alpha+beta)*transmission_l;

gamma=CA_module(im);
pmf=zeros(256,1,3);
for index=1:3
    [counts,x] = imhist(im(:,:,index));
    pmf(:,:,index)=counts./(height*width);
end

arg_max=zeros(3);%arg_max(1)��r, arg_max(2)��g, arg_max(3)��b

for index=1:3
    arg_max(index)=max(max(pmf(:,:,index)));
end

sigma=zeros(3);%sigma(1)��r, sigma(2)��g, sigma(3)��b
sigma(1)=arg_max(1);
sigma(2)=(arg_max(1)+arg_max(2))/2;
sigma(3)=(arg_max(2)+arg_max(3))/2;

J = a_getRadiance(A,im,transmission,gamma,sigma);

% figure;imagesc(JDark);colormap gray;colorbar;title('JDark')
% figure;imshow(JDark_s);title('JDark_s');
% figure;imshow(JDark_l);title('JDark_l');
% figure;imshow(transmission);title('transmission');
% figure;imshow(J);title('J');
