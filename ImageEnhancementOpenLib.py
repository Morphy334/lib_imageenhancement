# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 13:26:42 2018

@author: Felix.Syue
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2
import math

def aug_integral(ori):
    integrIma=np.zeros((ori.shape[0],ori.shape[1]),np.float64)
    integrIma[0,0]=ori[0,0]
    
    
    for i in range(1,ori.shape[0]):
        integrIma[i,0]=ori[i,0]+integrIma[i-1,0]
    for j in range(1,ori.shape[1]):
        integrIma[0,j]=ori[0,j]+integrIma[0,j-1]    
    
    for i in range(1,ori.shape[0]):
        for j in range(1,ori.shape[1]):
            integrIma[i,j]=ori[i,j]+integrIma[i-1,j]+integrIma[i,j-1]-integrIma[i-1,j-1]
    return integrIma

def GHE1992(ori):
    aug_j=0
    if ori.ndim==3 and ori.dtype==np.uint8:
        Lab=cv2.cvtColor(ori,cv2.COLOR_BGR2Lab)
        Lchannel=Lab[:,:,0]
        aug_j=1
    else:
        Lchannel=np.copy(ori)
        
    height=Lchannel.shape[0]
    width=Lchannel.shape[1]
    binNum=256
    pdf=np.zeros((binNum,),np.float64)
    cdf=np.zeros((binNum,),np.float64)
    for i in range(height):
        for j in range(width):
            pdf[Lchannel[i,j]]=pdf[Lchannel[i,j]]+1
    
    cdf[0]=pdf[0]
    for k in range(1,binNum):
        cdf[k]=pdf[k]+cdf[k-1]
    
    for i in range(height):
        for j in range(width):
            Lchannel[i,j]=cdf[Lchannel[i,j]]/(height*width)*(binNum-1)
        
    enhancedRet= cv2.cvtColor(Lab,cv2.COLOR_Lab2BGR) if aug_j else Lchannel 
    return enhancedRet

def MSRCR1997(ori,scale=(15,81,251)):
    assert (ori.ndim == 3),"input should be 3 dim(BGR)"
    
    colorDim=ori.ndim
    ori=np.float64(ori)
    alpha  = 128.0
    gain   = 1.0
    offset = 0.0
    cvar=1.2

    msrcrRet=np.zeros(ori.shape,ori.dtype)
    msr=np.zeros(ori.shape,np.float64)
    for c in range (colorDim):
        for i in scale:
            gBlur=cv2.GaussianBlur(ori[:,:,c],(i,i),0)
            msr[:,:,c]=(np.log(ori[:,:,c]+1)-np.log(gBlur+1))/len(scale)+msr[:,:,c]
        
    if colorDim==3:
        logl=np.log(ori[:,:,0]+ori[:,:,1]+ori[:,:,2]+3)
    else:
        logl=np.log(ori[:,:,0]+1)
    for c in range(colorDim):
        msrcrRet[:,:,c]=gain*((np.log(alpha*(ori[:,:,c]+1))-logl)*msr[:,:,c])+ offset
    
    
    mean=np.sum(msrcrRet[:,:,:])/(colorDim*ori.shape[0]*ori.shape[1])
    var=np.sum(msrcrRet[:,:,:]**2)/(colorDim*ori.shape[0]*ori.shape[1])
    var=(var-mean**2)**0.5
    mini = mean - cvar*var
    maxi = mean + cvar*var
    rangeV = maxi - mini
    msrcrRet=(msrcrRet-mini)/rangeV*255
    
    msrcrRet[msrcrRet>255]=255
    msrcrRet[msrcrRet<0]=0
    msrcrRet=np.uint8(msrcrRet)
    
    return msrcrRet 
def BBHE1997(ori):
    aug_j=0
    if ori.ndim==3 and ori.dtype==np.uint8:
        Lab=cv2.cvtColor(ori,cv2.COLOR_BGR2Lab)
        Lchannel=Lab[:,:,0]
        aug_j=1
    else:
        Lchannel=np.copy(ori)
   
    height=ori.shape[0]
    width=ori.shape[1]
    pdf=np.zeros((256,),np.float)
    cdf=np.zeros((256,),np.float)
    
    meanV=0
    for i in range(height):
        for j in range(width):
            meanV=meanV+Lchannel[i,j]
    
    meanV=int(meanV/(height*width)+0.5)
    nL=0
    nU=0
    
    
    for i in range(height):
        for j in range(width):
            pdf[Lchannel[i,j]] =pdf[Lchannel[i,j]]+1
            if Lchannel[i,j]<=meanV:
                nL =nL+1
            else:
                nU =nU+1
                
    cdf[0]=pdf[0]/nL
    cdf[meanV+1]=pdf[meanV+1]/nU
    for k in range(1,255+1):
        if k<=meanV:
            pdf[k]=pdf[k]/nL
            cdf[k]=cdf[k-1]+pdf[k]
        elif k==meanV+1:
            continue
        else :
            pdf[k]=pdf[k]/nU
            cdf[k]=cdf[k-1]+pdf[k]
    
        
    for i in range(height):
        for j in range(width):
            if Lchannel[i,j]<=meanV:
                Lchannel[i,j]=meanV*cdf[Lchannel[i,j]]
            else:
                Lchannel[i,j]=meanV+1+(255-(meanV+1))*cdf[Lchannel[i,j]]
                
    enhancedRet= cv2.cvtColor(Lab,cv2.COLOR_Lab2BGR) if aug_j else Lchannel 
    return enhancedRet
    
    
    
def RajuNair2014(ori):
    
    width=ori.shape[1]
    height=ori.shape[0]
    
    aug_j=0
    if ori.ndim==3 and ori.dtype==np.uint8:
        HSV=cv2.cvtColor(ori,cv2.COLOR_BGR2HSV)
        Vchannel=HSV[:,:,2]
        aug_j=1
    else:
        Vchannel=np.copy(ori)
        
    VchannelF = np.float32(Vchannel)/255
    
    # Expectation value
    expecValue=0
    
    for i in range(height):
        for j in range(width):
            expecValue=expecValue+VchannelF[i,j]
    expecValue=expecValue/(height*width)      
            
    #Enhancement
    K=0.5
    E=1
    for i in range(height):
        for j in range(width):
            if VchannelF[i,j]<expecValue:
                ud1=VchannelF[i,j]/expecValue
                Vchannel[i,j]=(ud1*K)*255
            else:
                ud2=(E-VchannelF[i,j])/(E-expecValue)
                Vchannel[i,j]=(E-ud2*K)*255
    
    enhancedRet= cv2.cvtColor(HSV,cv2.COLOR_HSV2BGR) if aug_j else Vchannel 
    return enhancedRet
  

def POHE2013(ori,bkSize):
    aug_j=0
    if ori.ndim==3 and ori.dtype==np.uint8:
        Lab=cv2.cvtColor(ori,cv2.COLOR_BGR2Lab)
        Lchannel=Lab[:,:,0]
        aug_j=1
    else:
        Lchannel=np.copy(ori)
    
    LchannelF=np.float32(Lchannel)
    halfW=np.int(bkSize[1]/2)
    halfH=np.int(bkSize[0]/2)
    pad_oriGray=cv2.copyMakeBorder(LchannelF,halfH,halfH+1,halfW+1,halfW,cv2.BORDER_REPLICATE)

    integrIma=aug_integral(pad_oriGray)
    integrImaVar=aug_integral(pad_oriGray**2)
    
    for i in range(halfH+1,halfH+ori.shape[0]):
        for j in range(halfW+1,halfW+ori.shape[1]):
            tpLeftX=j-halfW-1
            tpLeftY=i-halfH-1
            
            dwRightX=j+halfW
            dwRightY=i+halfH
            
            rectW=dwRightX-tpLeftX
            rectH=dwRightY-tpLeftY
            
            meanV=0
            meanV=integrIma[dwRightY,dwRightX]\
            -integrIma[tpLeftY,dwRightX]\
            -integrIma[dwRightY,tpLeftX]\
            +integrIma[tpLeftY,tpLeftX]
            meanV=meanV/(rectH*rectW)
            
            
            varV=0
            varV=integrImaVar[dwRightY,dwRightX]\
            -integrImaVar[tpLeftY,dwRightX]\
            -integrImaVar[dwRightY,tpLeftX]\
            +integrImaVar[tpLeftY,tpLeftX]
            varV=varV/(rectH*rectW)
            
            if varV-meanV**2 >0:
                std=(varV-meanV**2)**0.5
            else:
                cdfGaussian=1
                continue
            
            inGrayValue=LchannelF[i-(halfH+1),j-(halfW+1)]
            erfIn=(inGrayValue-meanV)/(2**0.5*std)
            
            erf=1-(0.254829592*(1/(1+ 0.3275911*abs(erfIn)))\
            -0.284496736*((1/(1+ 0.3275911*abs(erfIn)))**2)\
            +1.421413741*((1/(1+ 0.3275911*abs(erfIn)))**3)\
            -1.453152027*((1/(1+ 0.3275911*abs(erfIn)))**4)\
            +1.061405429*((1/(1+ 0.3275911*abs(erfIn)))**5)\
            )*math.e**(-erfIn**2)
            cdfGaussian=0.5*(1+erf) if erfIn>0 else 0.5*(1-erf)
            
            Lchannel[i-(halfH+1),j-(halfW+1)]=int(cdfGaussian*255)
            
    enhancedRet= cv2.cvtColor(Lab,cv2.COLOR_Lab2BGR) if aug_j else Lchannel 
    return enhancedRet

    
    
    
def LCE_BSESCS2014(ori,bkSize):    
    aug_j=0
    if ori.ndim==3 and ori.dtype==np.uint8:
        Lab=cv2.cvtColor(ori,cv2.COLOR_BGR2Lab)
        Lchannel=Lab[:,:,0]
        aug_j=1
    else:
        Lchannel=np.copy(ori)
    
    height=ori.shape[0]
    width=ori.shape[1]
    radiH=np.int(bkSize[0]/2)
    radiW=np.int(bkSize[1]/2)
    L=256
    enhancedRet=np.zeros(Lchannel.shape,Lchannel.dtype)

    for i in range(height):
        for j in range(width):
            tpL_y=i-radiH if i-radiH>0 else 0
            tpL_x=j-radiW if j-radiW>0 else 0
            dwR_y=i+radiH if i+radiH<height else height-1
            dwR_x=j+radiW if j+radiW<width else width-1
            bkRealWidth=dwR_x-tpL_x+1
            bkRealHeight=dwR_y-tpL_y+1
            cropImg=Lchannel[tpL_y:dwR_y+1,tpL_x:dwR_x+1]
            hist = cv2.calcHist([cropImg],[0],None,[L],[0,L])
            histCut=np.zeros(hist.shape,hist.dtype)
           
            mean=0.
            for k in range(L):
                mean=mean+k*hist[k]
            mean = math.floor(mean / (bkRealWidth*bkRealHeight)+0.5)
            TCR=0
            temp=0
            nT=0
            H_hatCDF=0
            if Lchannel[i,j]<=mean:
                for k in range(mean+1):
                    temp=temp+ hist[k]
                TCR = math.floor(temp/(mean+1))+1
                for k in range(mean+1):
                    histCut[k] =TCR if hist[k]>TCR else hist[k]
                    nT =nT+histCut[k]
                    if k==Lchannel[i,j]:
                        H_hatCDF=nT 
                Lchannel[i,j]=math.floor(mean/nT*H_hatCDF)
            else:
                for k in range(mean+1,L):
                    temp=temp+hist[k]
                TCR = math.floor(temp/(L-mean-1))+1
                for k in range(mean+1,L):
                    histCut[k] =TCR if hist[k]>TCR else hist[k]
                    nT =nT+histCut[k]
                    if  k ==  Lchannel[i,j]-1:
                        H_hatCDF=nT
                Lchannel[i,j]=math.floor((L-mean-2)/nT*(H_hatCDF))+mean+1
                
    enhancedRet= cv2.cvtColor(Lab,cv2.COLOR_Lab2BGR) if aug_j else Lchannel
    return enhancedRet    

def R_ESIHE2015(ori):
    aug_j=0
    if ori.ndim==3 and ori.dtype==np.uint8:
        Lab=cv2.cvtColor(ori,cv2.COLOR_BGR2Lab)
        Lchannel=Lab[:,:,0]
        aug_j=1
    else:
        Lchannel=np.copy(ori)
        
    enhancedRet=np.zeros(Lchannel.shape,Lchannel.dtype)
    L=256
    exposureOld=-1
    
    for k in range(20):
        hist = cv2.calcHist([Lchannel],[0],None,[L],[0,L])
        exposure=np.mean(Lchannel)/L
        if abs(exposure-exposureOld)<0.01:
            break
        else:
            exposureOld=exposure
            
        Xa=int(np.floor(L*(1-exposure)+0.5))
        Tc=int(np.floor(np.mean(hist)+0.5))
        
        hc=np.copy(hist)
        hc[hc>Tc]=Tc
        
        PL=np.copy(hc[0:Xa,0])
        PL=np.reshape(PL,(len(PL),1))
        PL=PL/np.sum(PL)
        
        PU=np.copy(hc[Xa:L,0])
        PU=np.reshape(PU,(len(PU),1))
        PU=PU/np.sum(PU)
        
        CL=np.zeros(PL.shape,PL.dtype)
        CU=np.zeros(PU.shape,PU.dtype)
        
        CL[0,0]=PL[0,0]
        for i in range(1,PL.shape[0]):
            CL[i,0]=CL[i-1,0]+PL[i,0]
            
        CU[0,0]=PU[0,0]
        for i in range(1,PU.shape[0]):
            CU[i,0]=CU[i-1,0]+PU[i,0]
            
        
        for i in range(Lchannel.shape[0]):
            for j in range(Lchannel.shape[1]):
                if Lchannel[i,j]<Xa:
                    Lchannel[i,j]=CL[Lchannel[i,j],0]*Xa
                else:
                    Lchannel[i,j]=Xa+(L-1-Xa)*CU[Lchannel[i,j]-Xa,0]
    

    enhancedRet= cv2.cvtColor(Lab,cv2.COLOR_Lab2BGR) if aug_j else Lchannel
    return enhancedRet
    
    
def RS_ESIHE2015(ori):
    aug_j=0
    if ori.ndim==3 and ori.dtype==np.uint8:
        Lab=cv2.cvtColor(ori,cv2.COLOR_BGR2Lab)
        Lchannel=Lab[:,:,0]
        aug_j=1
    else:
        Lchannel=np.copy(ori)
        
    L=256
    hist = cv2.calcHist([Lchannel],[0],None,[L],[0,L])
    exposure=np.mean(Lchannel)/L
    Xa=int(np.floor(L*(1-exposure)+0.5))
    
    #possibel to have denominator as 0, but the original paper did not handle this
    Xal=int(Xa-np.sum(hist[0:Xa,0]*np.arange(Xa))/np.sum(hist[0:Xa,0])+0.5)
    Xau=int(L+Xa-np.sum(hist[Xa:L,0]*np.arange(Xa,L))/np.sum(hist[Xa:L,0])+0.5)
    
    for i in range(ori.shape[0]):
        for j in range(ori.shape[1]):
            if Lchannel[i,j]<Xal:
                CLl=np.sum(hist[0:Lchannel[i,j]+1,0])/np.sum(hist[0:Xal,0])
                Lchannel[i,j]=Xal*CLl
            elif Lchannel[i,j]>=Xal and Lchannel[i,j]<Xa:
                CLu=np.sum(hist[Xal:Lchannel[i,j]+1,0])/np.sum(hist[Xal:Xa,0])
                Lchannel[i,j]=Xal+(Xa-Xal-1)*CLu
            elif Lchannel[i,j]>=Xa and Lchannel[i,j]<Xau:
                CUl=np.sum(hist[Xa:Lchannel[i,j]+1,0])/np.sum(hist[Xa:Xau,0])
                Lchannel[i,j]=Xa+(Xau-Xa-1)*CUl
            else:
                CUu=np.sum(hist[Xau:Lchannel[i,j]+1,0])/np.sum(hist[Xau:L,0])
                Lchannel[i,j]=Xau+(L-Xau-1)*CUu
    
    enhancedRet= cv2.cvtColor(Lab,cv2.COLOR_Lab2BGR) if aug_j else Lchannel
    return enhancedRet
    
if __name__=='__main__':

    readStr=r'./testImg/test5.jpg'
    
    
    
    ori=cv2.imread(readStr,-1)
    enhanced=GHE1992(ori)
    cv2.imwrite('GHE1992.png',enhanced)
    
    ori=cv2.imread(readStr,-1)
    enhanced=R_ESIHE2015(ori)
    cv2.imwrite('R_ESIHE2015.png',enhanced)
    
    ori=cv2.imread(readStr,-1)
    enhanced=RS_ESIHE2015(ori)
    cv2.imwrite('RS_ESIHE2015.png',enhanced)
    
    ori=cv2.imread(readStr,-1)
    bkSize=(117,117)
    enhanced=LCE_BSESCS2014(ori,bkSize)
    cv2.imwrite('LCE_BSESCS2014.png',enhanced)
    
    ori=cv2.imread(readStr,-1)
    enhanced=RajuNair2014(ori)
    cv2.imwrite('RajuNair2014.png',enhanced)
    
    
    
    ori=cv2.imread(readStr,-1)
    sizeBk=(117,117)
    enhanced=POHE2013(ori,bkSize)
    cv2.imwrite('POHE2013.png',enhanced)
    
    
    ori=cv2.imread(readStr,-1)
    scale=(15,81,251)
    enhanced=MSRCR1997(ori,scale)
    cv2.imwrite('MSRCR1997.png',enhanced)
    
    
    
    ori=cv2.imread(readStr,-1)
    enhanced=BBHE1997(ori)
    cv2.imwrite('BBHE1997.png',enhanced)
    
    
    
    
# =============================================================================
#     cv2.imshow('ori',ori)
#     cv2.imshow('enhanced',enhanced)
# =============================================================================
    
    cv2.waitKey(0)




