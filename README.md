# Image Enhancement OpenLib

Image Enhancement OpenLib, coded by C++, matlab, python, implemented by myself.

### Image contrast enhancement

* Global-based
  * R_ESIHE2015[1]
  * RS_ESIHE2015[2]
  * RajuNair2014[3]
  * LeeLeeKim2013[8]
  * BBHE1997[4]
  
* Local-based
  * LCE-BSESCS2014[5]
  * MSRCP2014[6]
  * POHE2013[7]
  * NRCIR2009[9]
  * MSRCR1997[10]

* Image defogging
  * AEVEA2014[11]
  * ISID_DMSR2010[12]
  
### Demo images:

![](./testImg/orilmgShow.jpg)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Original image**
<br>

![](./testImg/summary_3.jpg)

### Citations
[1]*K. Singh, R. Kapoor and S. K. Sinha, "Enhancement of low exposure images via recursive histogram equaliztion algorithms," Optik, vol. 126, no. 20, pp. 2619-2625, 2015.* <br>
[2]*K. Singh, R. Kapoor and S. K. Sinha, "Enhancement of low exposure images via recursive histogram equaliztion algorithms," Optik, vol. 126, no. 20, pp. 2619-2625, 2015.* <br>
[3]*G. Raju and M. S. Nair, "A fast and efficient color image enhancement method based on fuzzy-logic and histogram," International Journal of Electronics and Communications, 2014.*<br>
[4]*Yeong-Taeg Kim, “Contrast enhancement using brightness preserving BiHistogram equalization”, IEEE Trans. Consumer Electronics, vol. 43, no. 1, pp. 1-8, Feb. 1997.*<br>
[5]*H. Ibrahim and S. C. Hoo, “Local contrast enhancement utilizing bidirectional switching equalization of separated and clipped subhistograms,” Mathematical Problems in Engineering, vol. 2014, 2014.*<br>
[6]*Ana Bel´en Petro1, Catalina Sbert2, Jean-Michel Morel3 , "Multiscale Retinex," Image Processing On Line, 2014.*<br>
[7]*Y. F. Liu, J. M. Guo, B. S. Lai, and J. D. Lee, "High efficient contrast enhancement using parametric approximation," in Proc. IEEE ICASSP, pp. 2444-2448, 26-31 May 2013.*<br>
[8]*C. Lee, C. Lee, C.-S. Kim, “Contrast enhancement based on layered difference representation of 2D histograms,” IEEE Trans. Image Process., vol. 22, no. 12, pp. 5372-5384, Dec. 2013.*<br>
[9]*S. Chen and A. Beghdadi, “Natural rendering of color image based on retinex,” in Proc. IEEE Int. Conf. Image Process., Nov. 2009, pp. 1813–1816.*<br>
[10]*D. J. Jobson, Z.-U. Rahman, and G. A. Woodell, “A multiscale retinex for bridging the gap between color images and the human observation of scenes,” IEEE Trans. Image Process., vol. 6, no. 7, pp. 965–976, Jul. 1997.*<br>
[11]*S. C. Huang, B. H. Chen, and Y. J. Cheng, “An efficient visibility enhancement algorithm for road scenes captured by intelligent transportation systems,” IEEE Trans. Intell. Transp. Syst., vol. 15, no. 5, pp. 2321–2332, Oct. 2014.*<br>
[12]*B. Xie, F. Guo, and Z. Cai, “Improved single image dehazing using dark channel prior and multi-scale retinex,” in Proc. Int. Conf. Intell. Syst. Design Eng. Appl., Oct. 2010, pp. 848–851.*<br>