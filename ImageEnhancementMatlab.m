clear all;clc;close all
fog_in=imread('./testImg/test4.jpg');
figure;
imshow(fog_in);

cd './ImageEnhancementMatlab/ISID_DMSR2010' 
J = deHaze(fog_in,[3,13,41],1.2);
figure;
imshow(J);
imwrite(J,'deHaze.png');


% cd  './ImageEnhancementMatlab/AEVEA2014' 
% J = anEfficient(fog_in,3,45,0.1);
% imwrite(J,'anEfficient.png');
% figure;
% imshow(J);



